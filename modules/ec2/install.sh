#!/bin/bash
sudo yum install java-11-openjdk -y
sudo yum install unzip wget fontconfig -y
wget https://www.atlassian.com/software/jira/downloads/binary/atlassian-jira-software-9.9.0-x64.bin
sudo chmod a+x atlassian-jira-software-9.9.0-x64.bin
sudo ./atlassian-jira-software-9.9.0-x64.bin <<EOF
o
1
i
ya
y
EOF
