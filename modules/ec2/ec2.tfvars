project_name = [
  "audi-2nf"
]

image_id = [
  "ami-0230bd60aa48260c6"
]

instance_type = [
  "t2.nano"
]

name_prefix = [
  "app-launch-template"
]

key_name = [
  "key-pair-01"
]

connection_type = [
  "ssh"
]

connection_user = [
  "ec2-user"
]

connection_host = [
  "self.public_ip"
]
