project_name = [
  "audi-2nf"
]

environment = [
  "dev"
]

vpc_id = [
  "aws_vpc.main.id"
]

vpc_cidr = [
  "10.0.0.0/16"
]

public_subnet = {
  "us-east-1a" : "10.0.0.0/24",
  "us-east-1b" : "10.0.1.0/24"
}

private_subnet = {
  "us-east-1a" : "10.0.101.0/24",
  "us-east-1b" : "10.0.102.0/24"
}

availability_zones = [
  "us-east-1a",
  "us-east-1b"
]

cidr_block = [
  "0.0.0.0/0"
]

route_table_id = [
  "aws_route_table.main[each.key].id"
]

gateway_id = [
  "aws_internet_gateway.main.id"
]

subnet_id = [
  "aws_subnet.public_subnet.id"
]

load_balancer_type = [
  "application"
]

app_alb = [
  "app-alb"
]

alb_internal = [
  "false"
]

load_balancer_arn = [
  "aws_lb.main.arn"
]

alb_listener_port = [
  "80"
]

alb_listener_protocol = [
  "HTTP"
]

alb_listener_type = [
  "forward"
]

alb_target_group_arn = [
  "aws_lb_target_group.main.arn"
]

alb_target_group = [
  "alb-target-group"
]

 alb_target_group_port = [
  "80"
]

alb_target_group_protocol = [
  "HTTP"
]

app_autoscaling_group = [
  "app-autoscaling-group"
]

desired_capacity = [
  "2"
]

max_size = [
  "2"
]

min_size = [
  "2"
]

alb_security_group_name = [
  "alb-app-security-group"
]

app_security_group_name = [
  "app-instance-security-group"
]

alb_security_group = [
  "aws_security_group.alb_security_group.id"
]

app_security_group = [
  "aws_security_group.app_security_group.id"
]
