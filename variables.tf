variable "aws_access_key" {
  description = "AWS Access Key ID"
  type        = string
  default     = ""
}

variable "aws_secret_key" {
  description = "AWS Secret Key ID"
  type        = string
  default     = ""
}

variable "region" {
  description = "AWS Region"
  type        = string
  default     = "us-east-1"
}

variable "project_name" {
  description = "Name of Project"
  type        = string
  default     = "audi-2nf"
}

variable "environment" {
  description = "Environment Type"
  type        = string
  default     = "dev"
}

################################################################################
# VPC Module - Variables 
################################################################################

variable "vpc_id" {
  description = "The VPC to be deployed"
  type        = string
  default     = "aws_vpc.main.id"
}

variable "vpc_cidr" {
  description = "The VPC Network Range"
  type        = string
  default     = "10.0.0.0/16"
}

variable "public_subnet" {
  description = "A list of public subnets inside the VPC"
  type        = map(string)
  default = {
    "us-east-1a" : "10.0.0.0/24",
    "us-east-1b" : "10.0.1.0/24",
  }
}

variable "private_subnet" {
  description = "A list of private subnets inside the VPC"
  type        = map(string)
  default = {
    "us-east-1a" : "10.0.101.0/24",
    "us-east-1b" : "10.0.102.0/24",
  }
}

variable "availability_zones" {
  description = "Availability Zones used"
  default     = ["us-east-1a","us-east-1b"]
}


variable "cidr_block" {
  description = "CIDR Block to allow traffic via"
  type        = string
  default     = "0.0.0.0/0"
}

variable "route_table_id" {
  description = "The ID of the Routing Table"
  type        = string
  default     = "aws_route_table.main[each.key].id"
}

variable "gateway_id" {
  description = "Identifier of the VPC Internet Gateway"
  type        = string
  default     = "aws_internet_gateway.main.id"
}

variable "subnet_id" {
  description = "subnet ID which resources will be launched in"
  type        = string
  default     = "aws_subnet.public_subnet.id"
}

variable "load_balancer_type" {
  description = "The type of Load Balancer"
  type        = string
  default     = "application"
}

variable "app_alb" {
  description = "Name of Application Load Balancer"
  type        = string
  default     = "app-alb"
}

variable "alb_internal" {
  description = "Application Load Balancer Network Type"
  type        = string
  default     = "false"
}

variable "load_balancer_arn" {
  description = "Application Load Balancer ARN"
  type        = string
  default     = "aws_lb.main.arn"
}

variable "alb_listener_port" {
  description = "Application Load Balancer Listener Port"
  type        = string
  default     = "80"
}

variable "alb_listener_protocol" {
  description = "Application Load Balancer Listener Protocol"
  type        = string
  default     = "HTTP"
}

variable "alb_listener_type" {
  description = "Application Load Balancer Listener Type"
  type        = string
  default     = "forward"
}

variable "alb_target_group_arn" {
  description = "Application Load Balancer Target Group ARN"
  type        = string
  default     = "aws_lb_target_group.main.arn"
}

variable "alb_target_group" {
  description = "Application Load Balancer Target Group"
  type        = string
  default     = "alb-target-group"
}

variable "alb_target_group_port" {
  description = "Application Load Balancer Target Group Port"
  type        = string
  default     = "80"
}

variable "alb_target_group_protocol" {
  description = "Application Load Balancer Target Group Protocol"
  type        = string
  default     = "HTTP"
}

variable "app_autoscaling_group" {
  description = "Autoscaling Group Name"
  type        = string
  default     = "app-autoscaling-group"
}

variable "desired_capacity" {
  description = "The number of EC2 instances that should be running in the group"
  type        = string
  default     = "2"
}

variable "max_size" {
  description = "The maximum size of the autoscale group"
  type        = string
  default     = "2"
}

variable "min_size" {
  description = "The minimum size of the autoscale group"
  type        = string
  default     = "2"
}

variable "alb_security_group_name" {
  description = "Application Load Balancer App Security Group Name"
  type        = string
  default     = "alb-app-security-group"
}

variable "app_security_group_name" {
  description = "App Instance Security Group Name"
  type        = string
  default     = "app-instance-security-group"
}

variable "alb_security_group" {
  description = "Application Load Balancer Security Group"
  type        = string
  default     = "aws_security_group.alb_security_group.id"
}

variable "app_security_group" {
  description = "Application Security Group"
  type        = string
  default     = "aws_security_group.app_security_group.id"
}


################################################################################
# EC2 Module - Variables 
################################################################################

variable "image_id" {
  description = "Image ID"
  type        = string
  default     = "ami-0230bd60aa48260c6"
}

variable "instance_type" {
  description = "Type of EC2 instance"
  type        = string
  default     = "t2.nano"
}

variable "name_prefix" {
  description = "Name of Launch Template"
  type        = string
  default     = "app-launch-template"
}

variable "id_app" {
  description = "Launch Template ID"
  type        = string
  default     = "aws_launch_template.main.id"
}

variable "key_name" {
  description = "Name of the Private Key to be used for the EC2 Instance"
  type        = string
  default     = "key-pair-01"
}

variable "connection_type" {
  description = "The type of connection used for the EC2 Instance"
  type        = string
  default     = "ssh"
}

variable "connection_user" {
  description = "EC2 User"
  type        = string
  default     = "ec2-user"
}

variable "connection_host" {
  description = "Allows connection to the newly created EC2 Instances"
  type        = string
  default     = "self.public_ip"
}

